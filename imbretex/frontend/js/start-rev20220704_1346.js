(function( $ ) {
/**
 * START - ONLOAD - JS
 * -------------
 * 1. Hero Slider
 * 2. Social Sidebar Scroll
 * 3. Caculate Quote Top Right
 * 4. Flag Language List
 * 5. Show Sub-menu PC
 * 6. Show/Hide Module
 * 7. Perfect Scrollbar
 * 8. Show Search Category
 * 9. Range Price
 * 10. Custom Input Number
 * 11. Input Calendar
 * 12. Feature Slider
 * 13. Product Slider
 * 14. Color Slider
 * 15. Show Header Search Mobile
 * 16. Show Sub-menu Mobile
 * 17. Back Main Menu Mobile
 * 18. Show Menu Mobile
 * 19. Close Menu Mobile
 * 20. Show Category Filter Mobile
 * 21. Close Category Filter Mobile
 * 22. Show Menu Commandes Mobile
 * 23. Close Menu Commandes Mobile
 * 24. Show Search Commandes
 * 25. Event Sort Status SAV
 * 26. Set Heihgt Contact IMG
 * 27. Sticky Header
 * 28. Sticky Menu Left
 * 29. Feature Image Gallery
 * 30. Event click Show Profile
 * 31. Set width for main menu left
 * 32. Range Stock
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */
/**
 * 1. Hero Slider
 */
function heroSlider() {
    if(!$('.hero-slider').length) { return; }

    $('.hero-slider').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
    });
}
/**
 * 2. Social Sidebar Scroll
 */
function socialScroll() {
    if(!$('.social-list').length) { return; }

    // init
    socialResize();

    // window resize
    $( window ).resize(
        $.debounce(300, function(e){
            socialResize();
        })
    );
}
function socialResize() {
    var w_scroll_window = 0;
    if (navigator.appVersion.indexOf("Win")!=-1) {
        w_scroll_window = 20;
    }
    if(($(window).width() + w_scroll_window) > 768 ) {
        $(window).scroll(function(){
            var scrollTop  = $(window).scrollTop(),
                footer_h   = $(".footer-wrap").outerHeight() + $(".footer-copyright").outerHeight();

            if(scrollTop > $('.footer-wrap').offset().top - footer_h){
                $(".social-list").addClass('bottom');
            } else{
                $(".social-list").removeClass('bottom');
            }
        });
    } else {
        $(window).scroll(function(){
            var scrollTop  = $(window).scrollTop(),
                footer_h   = $(".footer-copyright").outerHeight() + 200;

            if(scrollTop > $('.footer-wrap').offset().top - footer_h){
                $(".social-list").addClass('bottom');
            } else{
                $(".social-list").removeClass('bottom');
            }
        });
    }
}
function showSocial() {
    if(!$('.link_share').length) { return; }

    $('.link_share').on('click', function(e) {
        e.preventDefault();
        var $a_share    =   $(this),
            $social     =   $a_share.siblings(".lst");

        if($a_share.hasClass('act')) {
            $a_share.removeClass('act');
            $social.removeClass('shw');
        } else {
            $a_share.addClass('act');
            $social.addClass('shw');
        }
    });

    // click out
    $(".social-list").bind( "clickoutside", function(event){
        $('.link_share').removeClass('act');
        $('.social-list .lst').removeClass('shw');
    });
}
/**
 * 3. Caculate Quote Top Right
 */
function calcQuoteTopRight() {
    if(!$('.quote.top-right').length) { return;}

    $('.quote.top-right').each(function(i) {
        var quote_w     =   $(this).outerWidth(),
            quote_h     =   $(this).outerHeight();
        $(this).css({
            'right': 0 - quote_w - quote_h + 80
        })
    });
}
/**
 *  4. Flag Language List
 */
function flagLangList() {
    if(!$('#chooseLang').length) { return; }

    $('#chooseLang').flagStrap({
        countries: {
            "fr": "Français",
            "gb": "English",
            "de": "Deutsch",
        },
        scrollable: false,
        scrollableHeight: "350px",
        buttonSize: "ipt-select ipt-lang",
        placeholder: {
            value: "",
            text: ""
        },
        onSelect: function (value, element) {
        }
    });
}
/**
 * 5. Show Sub-menu PC
 */
function showSubMenuPC() {
    if(!$(".dropdown-submenu").length) { return; }

    $('.main-menu .has-child').each(function(i) {
        $(this).hoverIntent({
            interval: 200,
            timeout: 200,
            over: function() {
                $(this).find('.dropdown-submenu').addClass('shw');
                $(this).closest('body').addClass('mn-submenu-open ');
            },
            out: function() {
                $(this).find('.dropdown-submenu').removeClass('shw');
                $(this).closest('body').removeClass('mn-submenu-open ');
            }
        });
    });
}
/**
 * 6. Show/Hide Module
 */
function showHideModule() {
    if(!$('.block-module').length) { return; }

    $('.mod-title .ttl').each(function(i) {
        $(this).on('click', function(e) {
            e.preventDefault();

            if($(this).closest('.block-module').hasClass('hiden')) {
                $(this).closest('.block-module').removeClass('hiden');
            } else {
                $(this).closest('.block-module').addClass('hiden');

                // hide search
                $(this).closest('.block-module').find('.category_filter').removeClass('act');
                $(this).closest('.block-module').find('.block-search-wrap').removeClass('shw');
            }
        })
    });
}
/**
 *  7. Perfect Scrollbar
 */
function perfectScrollbar() {
    if(!$('.mod-scrollbar').length) { return; }

    $('.mod-scrollbar').each(function(i) {
        $(this).mCustomScrollbar({
            theme:"dark"
        });
    });
}
/**
 * 8. Show Search Category
 */
function showSearchCategory() {
    if(!$('.category_filter').length) {return;}

    $('.category_filter').on('click', function(e) {
        e.preventDefault();

        // check close module
        if($(this).closest('.block-module').hasClass('active')) { return; }

        var $a_filter   =   $(this),
            $blk_search =   $a_filter.siblings('.block-search-wrap');

        if($a_filter.hasClass('act')) {
            $a_filter.removeClass('act');
            $blk_search.removeClass('shw');
        } else {
            $a_filter.addClass('act');
            $blk_search.addClass('shw');
        }
    });

    // click out
    $(".mod-category .mod-title ").bind( "clickoutside", function(event){
        $('.category_filter').removeClass('act');
        $('.mod-category .block-search-wrap').removeClass('shw');
    });
}
/**
 * 9. Range Price
 */
function rangePrice() {
    if(!$('#price-range').length) { return; }

    $('#price-range').slider()
    .on('slide', function(ev){
        $('#slider-to').val($('#slider-to').data('symboy') + ' ' + ev.value[0]);
        $('#slider-from').val($('#slider-from').data('symboy') + ' ' + ev.value[1]);
    });
}
/**
 * 10. Custom Input Number
 */
function customInputNumber() {
    if(!$('.custom-number').length) { return; }

    $('<div class="button-number up"><i class="ico_ico_arrow_up"></i></div><div class="button-number down"><i class="ico_ico_arrow_down"></i></div>').insertAfter('.custom-number .ipt_num');

    $('.custom-number').each(function(i) {
        var $input      =   $(this).find('.ipt_num')
            btn_down    =   $(this).find('.button-number.down'),
            btn_up      =   $(this).find('.button-number.up'),
            data_min    =   $input.attr('min'),
            data_max    =   $input.attr('max'),
            data_step   =   $input.attr('step');

        btn_up.on('click', function(e) {
            e.preventDefault();
            var oldValue = parseInt($input.val()) ? parseInt($input.val()) : 0;
            var newVal;
            if(oldValue >= data_max) {
                newVal = oldValue;
            } else {
                newVal  = oldValue + parseInt(data_step);
            }

            $input.val(newVal);
            $input.trigger("change");
        });

        btn_down.on('click', function(e) {
            e.preventDefault();
            var oldValue = parseInt($input.val()) ? parseInt($input.val()) : 0;
            var newVal;
            if(oldValue <= data_min) {
                newVal = 0;
            } else {
                newVal  = oldValue - parseInt(data_step);
            }

            $input.val(newVal);
            $input.trigger("change");
        })
    });
}
/**
 *  11. Input Calendar
 */
function inputCalendar() {
    if(!$('.ipt_calendar').length) { return; }

    $('.ipt_calendar').each(function(i) {
        $(this).datepicker({
            format: 'dd-mm-yyyy',
            language: 'fr-FR',
            autoHide: true
        });
    })
}
/**
 * 12. Feature Slider
 */
function featureSlider() {
    if(!$('.feature-prd-images').length
    || !$('.feature-nav-images').length) { return; }

    $('.feature-prd-images').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        infinite: false,
        asNavFor: '.feature-nav-images',
        responsive: [
            {
                breakpoint: 901,
                settings: {
                    dots: true
                }
            },
        ]
    });
    $('.feature-nav-images').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        asNavFor: '.feature-prd-images',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        arrows: false
    });
}
/**
 * 13. Product Slider
 */
function productSlider(objSlider) {
    if(!$(objSlider).length) {return;}

    $(objSlider).not('.slick-initialized').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        variableWidth: false,
        dots: true,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true,
        responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 4,
              }
            },
            {
                breakpoint: 861,
                settings: {
                  slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                }
            }
        ]
    });
}
function brandSlider(objSlider) {
    if(!$(objSlider).length) {return;}

    $(objSlider).not('.slick-initialized').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        variableWidth: false,
        dots: true,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true,
        responsive: [
            {
              breakpoint: 1025,
              settings: {
                slidesToShow: 4,
              }
            },
            {
                breakpoint: 861,
                settings: {
                  slidesToShow: 3,
                }
            },
            {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                }
            }
        ]
    });
}
/**
 * 14. Color Slider
 */
function colorSlider() {
    if(!$(".colors-slider-wrap.has-slider .color-list ").length) {return;}

    $(".colors-slider-wrap.has-slider .color-list ").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true
    });
}
/**
 * 15. Show Header Search Mobile
 */
function showHeaderSearchMB() {
    if(!$('.header-search-mobile').length
    || !$(".search_mb").length) { return; }

    // show search
    $(".search_mb").on('click', function(e) {
        e.preventDefault();
        var $a_search   =   $(this),
            $search_mb  =   $a_search.closest(".header-nav").siblings('.header-search-mobile'),
            $body       =   $a_search.closest('body');

        if($a_search.hasClass('act')) {
            $a_search.removeClass('act');
            $search_mb.removeClass('shw');
            $body.removeClass('mn-search-open');
        } else {
            $a_search.addClass('act');
            $search_mb.addClass('shw');
            $body.addClass('mn-search-open');

        }
    });

    // close search
    $('.close-search-mb').on('click', function(e){
        e.preventDefault();

        var $a_close    =   $(this),
            $search_mb  =   $a_close.closest('.header-search-mobile'),
            $body       =   $a_close.closest('body'),
            $a_search   =   $search_mb.siblings('.header-nav').find('.search_mb');

        $a_search.removeClass('act');
        $search_mb.removeClass('shw');
        $body.removeClass('mn-search-open');
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.search_mb')
            || $(e.target).is('.search_mb *')
            || $(e.target).is('.header-search-mobile')
            || $(e.target).is('.header-search-mobile *')) { return; }

            $('body').removeClass('mn-search-open');
            $('.header-search-mobile').removeClass('shw');
            $('.search_mb').removeClass('act');
    });
}
/**
 * 16. Show Sub-menu Mobile
 */
function showSubMenuMB() {
    if(!$(".sub_link").length) { return; }

    $(".sub_link").each(function(i) {
        $(this).on('click', function(e) {
            e.preventDefault();
            var $a_link         =   $(this),
                menu_id         =   $a_link.data('grid'),
                $blk_menu       =   $a_link.closest('.mobile-content-menu');

            $blk_menu.addClass('mn-hide');
            $('#' + menu_id).addClass('shw');
            // scroll up
            $('.header-menu-mobile').animate({
                scrollTop: 0
            },{
                queue: false,
                duration: 1000
            });
        })
    });
}
/**
 * 17. Back Main Menu Mobile
 */
function backMainMenuMB() {
    if(!$('.back-link').length) { return; }

    $('.back-link').each(function(i) {
        $(this).on('click', function(e) {
            e.preventDefault();
            var $a_back     =   $(this),
                $sub_menu   =   $a_back.closest('.sub-menu-mb'),
                $mainmenu   =   $a_back.closest('.mobile-sub-menu').siblings('.mobile-content-menu');

            $mainmenu.removeClass('mn-hide');
            $sub_menu.removeClass('shw');
        });
    });
}
/**
 * 18. Show Menu Mobile
 */
function showMenuMB() {
    if(!$('.menu_mb').length
    || !$(".header-menu-mobile").length) { return; }

    $('.menu_mb').on('click', function(e) {
        e.preventDefault();

        var $a_menu     =   $(this),
            $body       =   $a_menu.closest('body'),
            $menu_mb    =   $a_menu.closest('.header-nav').siblings('.header-menu-mobile');

        $body.addClass('mn-open');
        $menu_mb.addClass('shw');
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.menu_mb')
            || $(e.target).is('.menu_mb *')
            || $(e.target).is('.header-menu-mobile')
            || $(e.target).is('.header-menu-mobile *')
            || $(e.target).is('#modalLang')
            || $(e.target).is('#modalLang *')) { return; }

            $('body').removeClass('mn-open');
            $('.header-menu-mobile').removeClass('shw');

            // reset menu
            $(".sub-menu-mb").removeClass('shw');
            $(".mobile-content-menu").removeClass('mn-hide');
    });
}
/**
 *  19. Close Menu Mobile
 */
function closeMenuMB() {
    if(!$('.mb-close').length) { return; }

    $('.mb-close').on('click', function(e) {
        e.preventDefault();

        var $a_menu     =   $(this),
            $body       =   $a_menu.closest('body'),
            $menu_mb    =   $a_menu.closest('.header-menu-mobile');

        $body.removeClass('mn-open');
        $menu_mb.removeClass('shw');

        // reset menu
        $(".sub-menu-mb").removeClass('shw');
        $(".mobile-content-menu").removeClass('mn-hide');
    });
}
/**
 * 20. Show Category Filter Mobile
 */
function showCateFilterMB() {
    if(!$('.filter_link').length
    || !$(".category-wrap .col-left").length) { return; }

    $('.filter_link').on('click', function(e) {
        e.preventDefault();

        var $a_filter     =   $(this),
            $body         =   $a_filter.closest('body'),
            $filter_blk   =   $a_filter.closest('.col-right').siblings('.col-left');

            $filter_blk.addClass('shw');
            $body.addClass('mn-filter-open');
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.filter_link')
            || $(e.target).is('.filter_link *')
            || $(e.target).is('.category-wrap .col-left')
            || $(e.target).is('.category-wrap .col-left *')) { return; }

            $('body').removeClass('mn-filter-open');
            $('.category-wrap .col-left').removeClass('shw');
    });
}
/**
 * 21. Close Category Filter Mobile
 */
function closeFilterMB() {
    if(!$('.close_filter').length) { return; }

    $('.close_filter').on('click', function(e) {
        e.preventDefault();

        var $a_close     =   $(this),
            $body       =   $a_close.closest('body'),
            $menu_mb    =   $a_close.closest('.col-left');

        $body.removeClass('mn-filter-open');
        $menu_mb.removeClass('shw');
    });
}
/**
 * 22. Show Menu Commandes Mobile
 */
function showCommandesInfoMB() {
    if(!$('.commandes-menu-link').length
    || !$(".commandes-wrap .comm-left").length) { return; }

    $('.commandes-menu-link').on('click', function(e) {
        e.preventDefault();

        var $a_filter     =   $(this),
            $body         =   $a_filter.closest('body'),
            $filter_blk   =   $a_filter.closest('.commandes-search').siblings('.commandes-wrap').find('.comm-left');

            $filter_blk.addClass('shw');
            $body.addClass('mn-commandes-open');
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.commandes-menu-link')
            || $(e.target).is('.commandes-menu-link *')
            || $(e.target).is('.commandes-wrap .comm-left')
            || $(e.target).is('.commandes-wrap .comm-left *')
            || $(e.target).is('.profile.login')
            || $(e.target).is('.profile.login *')) { return; }

            $('body').removeClass('mn-commandes-open');
            $('.commandes-wrap .comm-left').removeClass('shw');
    });
}
/**
 * 23. Close Menu Commandes Mobile
 */
function closeCommandesInfoMB() {
    if(!$('.close_commandes_info').length) { return; }

    $('.close_commandes_info').on('click', function(e) {
        e.preventDefault();

        var $a_close     =   $(this),
            $body       =   $a_close.closest('body'),
            $menu_mb    =   $a_close.closest('.comm-left');

        $body.removeClass('mn-commandes-open');
        $menu_mb.removeClass('shw');
    });
}
/**
 *  24. Show Search Commandes
 */
function showSearchCommande() {
    if(!$(".link_mod_search").length
    || !$('.commandes-search .block-search-wrap')) { return; }

    $(".link_mod_search").on('click', function(e) {
        e.preventDefault();
        var $a_search       =   $(this),
            $icon           =   $a_search.find('.ico')
            $blk_search     =   $a_search.siblings('.block-search-wrap');

        if($a_search.hasClass('active')) {
            $a_search.removeClass('active');
            $blk_search.removeClass('shw');
            // change icon
            $icon.addClass('search');
            $icon.removeClass('close');
        } else {
            $a_search.addClass('active');
            $blk_search.addClass('shw');
            // change icon
            $icon.removeClass('search');
            $icon.addClass('close');
        }
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.link_mod_search')
            || $(e.target).is('.link_mod_search *')
            || $(e.target).is('.commandes-search .block-search-wrap')
            || $(e.target).is('.commandes-search .block-search-wrap *')) { return; }

            $('.link_mod_search').removeClass('act');
            $('.commandes-search .block-search-wrap').removeClass('shw');
            // change icon
            $('.link_mod_search .ico').addClass('search');
            $('.link_mod_search .ico').removeClass('close');
    });
}
/**
 * 25. Event Sort Status SAV
 */
function sortStatusSAV() {
    if(!$(".sav-tbl .th-status").length) { return; }

    $(".sav-tbl .th-status").on('click', function(e) {
        e.preventDefault();
        var $a_status   =   $(this),
            $icon       =   $a_status.find('.ico');

        if($a_status.hasClass('act')) {
            $a_status.removeClass('act');
            // change icon
            $icon.addClass('arrow_down');
            $icon.removeClass('arrow_up');
        } else {
            $a_status.addClass('act');
            // change icon
            $icon.removeClass('arrow_down');
            $icon.addClass('arrow_up');
        }
    });
}
/**
 * 26. Set Heihgt Contact IMG
 */
function setHeightContactIMG() {
    if(!$(".contact-img-pc").length
    || !$(".contact-wrap .inner-form").length
    || $(window).width() < 993) { return; }

    var contact_form_h = $(".contact-wrap .inner-form").outerHeight(),
        button_h       = $('.contact-button').outerHeight();
    $(".contact-img-pc").css('height', contact_form_h - button_h - 20);

    // window resize
    $( window ).resize(
        $.debounce(300, function(e){
            var contact_form_h = $(".contact-wrap .inner-form").outerHeight(),
                button_h       = $('.contact-button').outerHeight();
            $(".contact-img-pc").css('height', contact_form_h - button_h - 20);
        })
    );
}
/**
 * 27. Sticky Header
 */
function stickyHeader() {
    if(!$(".header-nav").length) { return; }

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop();

        if(scrollTop >= $('.main').offset().top ){
            $(".header-nav").addClass('fixed');
        } else{
            $(".header-nav").removeClass('fixed');
        }
    });
}
/**
 * 28. Sticky Menu Left
 */
function stickyMenuLeft() {
    if(!$(".commandes-wrap .comm-left .box-white").length
    || $(window).width() < 993) {return;}

    $(window).scroll(function(){
        var scrollTop  = $(window).scrollTop(),
            footer_h   = $(".footer-wrap").outerHeight() + $(".footer-copyright").outerHeight() + 200;

        // sticky
        if(scrollTop >= $('.commandes-wrap').offset().top ){
            $(".commandes-wrap .comm-left .box-white").addClass('fixed');
        } else{
            $(".commandes-wrap .comm-left .box-white").removeClass('fixed');
        }

        // remove sticky when scroll down footer
        if(scrollTop > $('.footer-wrap').offset().top - footer_h){
            $(".commandes-wrap .comm-left .box-white").addClass('bottom');
        } else{
            $(".commandes-wrap .comm-left .box-white").removeClass('bottom');
        }
    });
}
/**
 * 29. Feature Image Gallery
 */
function featureLightGallery() {
    if(!$(".prd-image-item").length) { return; }

    $('.feature-prd-images').lightGallery({
        selector: '.prd-image-item',
        zoom: false,
        share: false,
        autoplay: false,
        download: false,
        autoplayControls: false,
        mode:"slide",
        speed : 400
    });
}
/**
 * 30. Event click Show Profile
 */
function showProfile() {
    if(!$('.profile.login').length
    || !$(".commandes-wrap .comm-left").length
    || $(window).width() > 992) { return; }

    $('.profile.login').on('click', function(e) {
        e.preventDefault();
        var $a_profile  =   $(this),
            $body       =   $a_profile.closest('body'),
            $profile    =   $a_profile.closest('.header-wrap').siblings('.main').find(".commandes-wrap .comm-left");

        $profile.addClass('shw');
        $body.addClass('mn-commandes-open');
    });

    // click out
    $(document).on('click', function (e) {
        if($(e.target).is('.commandes-menu-link')
            || $(e.target).is('.commandes-menu-link *')
            || $(e.target).is('.commandes-wrap .comm-left')
            || $(e.target).is('.commandes-wrap .comm-left *')
            || $(e.target).is('.profile.login')
            || $(e.target).is('.profile.login *')) { return; }

            $('body').removeClass('mn-commandes-open');
            $('.commandes-wrap .comm-left').removeClass('shw');
    });
}
/**
 *  31. Set width for main menu left
 */
function setWidthMainMenuLeft() {
    if(!$(".header-logo-w .main-menu.left").length
    || $(window).width() < 993) { return; }

    var ulWi = 0 ;
    $(".header-logo-w .main-menu.left > li").each(function(i) {
        ulWi += $(this).outerWidth(true);
    });
    $(".header-logo-w .main-menu.left").css('width', ulWi + 15);

    // window resize
    $( window ).resize(
        $.debounce(300, function(e){
            var ulWi = 0 ;
            $(".header-logo-w .main-menu.left > li").each(function(i) {
                ulWi += $(this).outerWidth(true);
            });
            $(".header-logo-w .main-menu.left").css('width', ulWi + 15);
        })
    );

}
/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    // 1.
    heroSlider();
    // 2.
    // socialScroll();
    showSocial();
    // 3.
    calcQuoteTopRight();
    // 4.
    flagLangList();
    // 5.
    showSubMenuPC();
    // 6.
    showHideModule();
    // 7.
    perfectScrollbar();
    // 8.
    showSearchCategory();
    // 9.
    rangePrice();
    // 10.
    customInputNumber();
    // 11.
    inputCalendar();
    // 12.
    featureSlider();
    // 13.
    brandSlider('.brand-list');
    productSlider('.associes-list');
    // check change tab
    $('#duo-tab').on('shown.bs.tab', function (e) {
        productSlider('.duo-list');
    });
    $('#similaires-tab').on('shown.bs.tab', function (e) {
        productSlider('.similaires-list');
    });
    // 14.
    colorSlider();
    // 15.
    showHeaderSearchMB();
    // 16.
    showSubMenuMB();
    // 17.
    backMainMenuMB();
    // 18.
    showMenuMB();
    // 19.
    closeMenuMB();
    // 20.
    // showCateFilterMB(); Moved to public/vendor/hegyd/products/js/frontend/common/filters.js
    // 21.
    closeFilterMB();
    // 22.
    showCommandesInfoMB();
    // 23.
    closeCommandesInfoMB();
    // 24.
    showSearchCommande();
    // 25.
    sortStatusSAV();
    // 26.
    setHeightContactIMG();
    // 27.
    stickyHeader();
    // 28.
    stickyMenuLeft();
    // 29.
    featureLightGallery();
    // 30.
    showProfile();
    // 31.
    setWidthMainMenuLeft();
    //32
    rangeStock();

    // change class body
    $('.modal-imb').on("shown.bs.modal", function() {
        HegydCore.initFileInput();
        $("body").addClass("modal-open");
    });
    $('.modal-imb').on("hidden.bs.modal", function() {

        $("body").removeClass("modal-open");
    });
    // js tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $('#lang-button').on('click',function(e){

        var $this = $(this);
        var locale = document.getElementById('locale').value;
        var devise = document.getElementById('slDevise').value;
        var fieldsDatas = { locale: locale, devise : devise };
        var priceVisibilityElem = document.getElementById('priceVisibility');
        if (priceVisibilityElem){
            fieldsDatas["price_visibility"] = priceVisibilityElem.value;
        }

        $.ajax({
            url: $this.attr('href'),
            method: 'get',
            data: fieldsDatas
        }).complete(function () {
            window.onbeforeunload = null;
            window.location.reload();
            toastr.success('Préférences mises à jour');
        })
            // .error(function () {
            //     toastr.error('Une erreur est survenue');
            // });
    });
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;
})(jQuery);


$('body').on('click','toggle-sub-size-20 , .tailles-20',function(){
    $('.sub-size-20').toggle();
    if( document.getElementById("tailles_20").checked == true){
        $(':checkbox.checkbox-20').click();
    }else{
        $(':checkbox.checkbox-20:checked').click();
    }

});
$('body').on('click','toggle-sub-size-30 , .tailles-30',function(){

    $('.sub-size-30').toggle();
    if( document.getElementById("tailles_30").checked == true){
        $(':checkbox.checkbox-30').click();
    }else{
        $(':checkbox.checkbox-30:checked').click();
    }

});

$('.toggle-sub-size').click(function(){
    $('.sub-size').toggle();
  });

  $('.slider-detail-kit').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

/**
 * 32. Range Price
 */
function rangeStock() {
    if(!$('#stock-range').length) { return; }

    $('#stock-range').slider()
        .on('slide', function(ev){
            $('#stock-slider-to').val($('#stock-slider-to').data('symboy') + ' ' + ev.value[0]);
            $('#stock-slider-from').val($('#stock-slider-from').data('symboy') + ' ' + ev.value[1]);
        });
}