$('.image-link').parent('div').find('img').on('click', function(){
    if( $(this).parent('div').find('.image-link').attr('data-link') ){
        window.open($(this).parent('div').find('.image-link').attr('data-link'), '_blank');
    }
});

$('.image-link').parent('div').find('img').css('cursor', 'pointer');

$('.video-img').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    var videoLink = $this.parent().find('.video-link').attr('data-link');

    lity(videoLink);

    return false;
});
