$(document).ready(function($){

    $('.select-color').on("change", function (e) {
        // console.log($(this).val());
        // console.log($(this).data('href'));
        var data = {
            'color_id': $(this).val(),
        };
        HegydCore.sendRequest($(this).data('href'), 'GET',data,
            function (data) {
                $('#'+"select-size-kit-"+data['item_id']).empty().append(data['view']);

            });
    });

    $('body').on('click','button.add-to-cart', function (e){
        e.preventDefault();

        $('.add-to-cart').prop('disabled', true);
        var treatment = document.getElementById('on-going-treatment').value;
        $('.add-to-cart').empty().append('<i class="fa fa-spinner fa-spin"></i>'+ treatment+'');
        addCart();
    });
    function addCart()
    {
        // console.log('add-to-cart');
        var json = [];
        var product_id = [];
        var quantity = [];

        //  console.log(directory_field);
        $('.select-size').each(function( index ) {

            if ($(this).val() > 0) {
                item = {'product_id': $(this).val(), 'quantity': $(this).data('quantity')};
                json.push(item);
                product_id.push( $(this).val());
                quantity.push( $(this).data('quantity'));
            }
        });
         // console.log(product_id,quantity);

        var token = $('meta[name="csrf-token"]').attr('content');

        var url = '/panier/ajout';
        if(quantity.length > 0)
        {
            $.ajax({
                url: url,
                method: 'POST',
                data: {'_token': token,
                    'product_id': product_id,
                    'is_kit': true,
                    'kit_id': $('#kit-name').data('kit-id'),
                    'quantity': quantity,
                    'directory_field':$('#kit-name').val(),
                    'return_templates': ['cart-header','popup-cart-detail','kit-add-to-cart']
                },
            }).success(function (data) {
                toastr.options =  { positionClass : "toast-top-right"};
                var message = document.getElementById('add-kit-cart').value;
                toastr.success(message);
                HegydEcommerce.updateCartViews(data);

            }).error(function (data) {

            });
        }
    }

});

