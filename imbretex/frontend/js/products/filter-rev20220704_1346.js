$(document).ready(function($){

    // $('#trier').change(function(){
    //     getFilter();
    // });
    $('.mod-title .ttl').each(function(i) {

                $(this).closest('.block-module-hidden').addClass('hiden');

                // hide search
                $(this).closest('.block-module-hidden').find('.category_filter').removeClass('act');
                $(this).closest('.block-module-hidden').find('.block-search-wrap').removeClass('shw');
    });

    $('#category_filter').click(function(){
        getFilterCategory();

        return false;
    });

    $('#category_keyword').keyup(function(e){
        if(e.keyCode == 13) {
            getFilterCategory();
        }

        return false;
    });

});

function getFilter()
{
    var url = '/marques/filter';

    $.ajax({
        url: url,
        method: 'GET',
        data: {},
    }).success(function (data) {
        if (data) {
            $('.product-list-wrap').html(data);
        }
    }).error(function (data) {
        //data
    });
}

function getFilterCategory()
{
    var url = '/produits/search-category';
    var keyword = $('#category_keyword').val();
    var checked = getChecked();

    $.ajax({
        url: url,
        method: 'GET',
        data: {'keyword': keyword, 'checked': checked},
    }).success(function (data) {
        if (data) {
            $('#filter_category').html(data);
            perfectScrollbar();
        }
    }).error(function (data) {
        //data
    });

}

function getChecked()
{
    var checked = [];
    $.each($("input[name='category']:checked"), function(){            
        checked.push($(this).val());
    });

    return checked;
}

function perfectScrollbar() {
    if(!$('.mod-scrollbar').length) { return; }

    $('.mod-scrollbar').each(function(i) {
        $(this).mCustomScrollbar({
            theme:"dark"
        });
    });
}