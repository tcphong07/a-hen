let gallerySlider = false;

jQuery(document).ready(function ($) {

    $(".item-news-home").click(function (e) {
        e.preventDefault();

        var url = $(this).find('a').data('url');
        if (typeof url !== 'undefined') {
            viewNews(url);
        }
    });

    $(".content-tienich ul li ").click(function () {
        var img = $(this).data('img');
        if (img) {
            $('.popup-tienich-noikhu .item img').attr('src', img);
            $('.popup-tienich-noikhu').addClass("active");
        }
    });
    $(".close-popup img").click(function () {
      $('.popup-tienich-noikhu').removeClass("active");
    });

    $(".room-1").click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');
        if (typeof url !== 'undefined') {
            $.ajax({
              url: url,
              type: 'GET',
              success: function(data) {
                $('.popu-matbang-khoi .content-popup').html(data);
                $('.popu-matbang-khoi').addClass("active");
                startRoom();

                $('.owl-nav').removeClass('disabled');
              },
              error: function() {
                 // error
              },
            });
        }

        return false;
    });
    $(".close-popup img").click(function () {
        $('.popu-matbang-khoi').removeClass("active");
    });

    $('.popu-matbang-khoi').on('click', '.owl-next', function(){
        let url = $('.popu-matbang-khoi .infor-popup').data('next');
        if (url) {
            viewRome(url);
        }
    });
    $('.popu-matbang-khoi').on('click', '.owl-prev', function(){
        let url = $('.popu-matbang-khoi .infor-popup').data('prev');
        if (url) {
            viewRome(url);
        }
    });

    startGallerySlider('.gallery01');
    let gallery02 = false;
    let gallery03 = false;
    $(".tab1-3").click(function () {
        $("#content-tab1-3, .tab1-3").addClass("active");
        $(".tab2-3,.tab3-3, #content-tab2-3, #content-tab3-3").removeClass("active");
    });
    $(".tab2-3").click(function () {
        $("#content-tab2-3, .tab2-3").addClass("active");
        $(".tab1-3,.tab3-3, #content-tab1-3, #content-tab3-3").removeClass("active");

        if (!gallery02) {
            startGallerySlider('.gallery02');
            gallery02 = true;
        }
    });
    $(".tab3-3").click(function () {
        $("#content-tab3-3, .tab3-3").addClass("active");
        $(".tab2-3,.tab1-3, #content-tab2-3, #content-tab1-3").removeClass("active");

        if (!gallery03) {
            startGallerySlider('.gallery03');
            gallery03 = true;
        }

    });

    $("#submit").click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#submit").attr('disabled', true);

        $.ajax({
          url: url,
          type: 'POST',
          data: $('#form-contact').serialize(),
          success: function(data) {
            if (data.error) {
                alert('Vui lòng điền đầy đủ thông tin.');
                $("#submit").attr('disabled', false);
            }
            else {
                $('input').val('');
                $('#content').val('');

                window.location.href = "/thank-you";
            }
          },
          error: function() {
             $("#submit").attr('disabled', false);
          },
        });
        return false;
    });
/*    $(".close-popup img").click(function () {
      $('.popup-thankyou').removeClass("active");
    });*/

    $('.icon-social a img').click(function(){
        var url = $(this).parent().attr('href');
        window.open(url);
    })

});

function startGallerySlider(element)
{
    $(element).fSlider({
          numOfNextSlides: 1,
          slidesToShow: 1,
          showArrows: false,
          autoplay: true,
          responsive: false,
          centerMode: true
       });
}

function startRoom()
{
  $('.room-slide').owlCarousel({
    center: true,
    items: 1,
    loop: true,
    nav: true,
    margin: 10,
    responsive: {
      600: {
        items: 1
      }
    }
  });
}

function viewNews(url)
{
    $.ajax({
      url: url,
      type: 'GET',
      success: function(data) {
        $('#news-content').html(data);
      },
      error: function() {
         // error
      },
    });
}

function viewRome(url)
{
    $.ajax({
      url: url,
      type: 'GET',
      success: function(data) {
        $('.popu-matbang-khoi .content-popup').html(data);
        $('.popu-matbang-khoi').addClass("active");
        startRoom();

        $('.owl-nav').removeClass('disabled');
      },
      error: function() {
         // error
      },
    });
}
