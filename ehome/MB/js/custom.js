let gallerySlider = false;

jQuery(document).ready(function ($) {

    // news
    $(".mb-news-item").click(function(e){
      $(".popup-blogs").addClass("active");

        e.preventDefault();

        var url = $(this).find('a').data('url');
        if (typeof url !== 'undefined') {
            viewNews(url);
        }

      $("body").addClass("head-popup");
    });

    $(".item-news-mb").click(function(e){
        e.preventDefault();

        var url = $(this).find('a').data('url');
        if (typeof url !== 'undefined') {
            viewNews(url);
        }
    });

    // Tien ich
    $(".tienich-content .content-tienich span ").click(function () {
        var img = $(this).data('img');
        if (img) {
            $('.popup-tienich .item img').attr('src', img);
            $('.popup-tienich').addClass("active");
            $("body").addClass("head-popup");
        }
    });

    // Mat bang
    $(".mb-room-list p").click(function(e){
        e.preventDefault();
        var url = $(this).data('url');
        if (typeof url !== 'undefined') {
            viewRome(url);
        }
    });

    $('.popup-block-a5').on('click', '.owl-next', function(){
        let url = $('.popup-block-a5 .infor-popup').data('next');
        if (url) {
            viewRome(url);
        }
    });
    $('.popup-block-a5').on('click', '.owl-prev', function(){
        let url = $('.popup-block-a5 .infor-popup').data('prev');
        if (url) {
            viewRome(url);
        }
    });

    $(".popup-block-a5").on('click', '.btn-close', function(){
        $(".popup-body").removeClass("active");
        $("body").removeClass("head-popup");
    });


    startGallerySlider('.gallery1');
 /*   let gallery2 = false;
    let gallery3 = false;
    $(".gallery-tab-1").click(function(){
        $(".mb-gallery-1").addClass("active");
        $(".mb-gallery-2, .mb-gallery-3").removeClass("active");
    });

    $(".gallery-tab-2").click(function(){
        $(".mb-gallery-2").addClass("active");
        $(".mb-gallery-1, .mb-gallery-3").removeClass("active");
        if (!gallery2) {
            startGallerySlider('.gallery2');
            gallery2 = true;
        }
    });

    $(".gallery-tab-3").click(function(){
        $(".mb-gallery-3").addClass("active");
        $(".mb-gallery-2, .mb-gallery-1").removeClass("active");
        if (!gallery3) {
            startGallerySlider('.gallery3');
            gallery3 = true;
        }
    });*/



    $("#submit").click(function (e) {
        e.preventDefault();
        var url = $(this).data('url');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#submit").attr('disabled', true);

        $.ajax({
          url: url,
          type: 'POST',
          data: $('#form-contact').serialize(),
          success: function(data) {
            if (data.error) {
                alert('Vui lòng điền đầy đủ thông tin.');
                $("#submit").attr('disabled', false);
            }
            else {
                $('input').val('');
                $('#content').val('');
                //$('.popup-thanks').show();
                window.location.href = "/thank-you";
            }
          },
          error: function() {
             $("#submit").attr('disabled', false);
          },
        });
        return false;
    });

    /*$(".popup-thanks").on('click', '.btn-close', function(){
        $('.popup-thanks').hide();
    });*/

});

function startGallerySlider(element)
{
/*     $(element).owlCarousel({
           autoplay: false,
           center: true,
           loop: true,
           nav: true,
           responsive:{
                0:{
                      items:1
                },
                600:{
                      items:2
                },
                1000:{
                      items:3
                }
            }
    });*/

     $('.gallery1,.gallery2,.gallery3').owlCarousel({
       autoplay: false,
       center: true,
       loop: true,
       nav: true,
       responsive:{
        0:{
              items:1
        },
        600:{
              items:2
        },
        1000:{
              items:3
        }
     }
       });
}

function startRoom()
{
  $('.mb-slider-1').owlCarousel({
        center: true,
        items: 1,
        loop: true,

        margin: 10,
        responsive: {
          600: {
            items: 1
          }
        }
  });
}

function viewNews(url)
{
    $.ajax({
      url: url,
      type: 'GET',
      success: function(data) {
        $('.news-content-popup #style-1').html(data);
      },
      error: function() {
         // error
      },
    });
}

function viewRome(url)
{
    $.ajax({
      url: url,
      type: 'GET',
      success: function(data) {
        $('.popup-block-a5 .popup-content').html(data);
        $(".popup-block-a5").addClass("active");
        $("body").addClass("head-popup");
        startRoom();

        $('.owl-nav').removeClass('disabled');
      },
      error: function() {
         // error
      },
    });
}
